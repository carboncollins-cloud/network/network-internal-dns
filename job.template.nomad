job "network-internal-dns" {
  name = "Network Internal DNS (PiHole & Unbound)"
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "default"

  priority = 90

  vault {
    policies = ["job-network-internal-dns"]
  }

  group "dns" {
    count = 2

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    constraint {
      attribute = "${attr.unique.hostname}"
      operator  = "set_contains_any"
      value     = "proxima-a,proxima-c"
    }

    constraint {
      distinct_hosts = true
    }

    network {
      mode = "bridge"

      port "dns" { to = "53" }
      port "http" { to = "80" }

      port "unbound" {}
    }

    service {
      name = "internal-dns"
      port = "80"
      task = "pi-hole"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "pi-hole" {
      driver = "docker"
      leader = true

      resources {
        cpu = 500
        memory = 300
      }

      config {
        image = "[[ .piholeImage ]]"

        ports = ["dns", "http", "telnet"]

        volumes = [
          "local/pihole/:/etc/pihole/",
          "local/dnsmasq.d/:/etc/dnsmasq.d/"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "jobs/network-internal-dns/tasks/pi-hole/web-ui" }}
          TZ='Europe/Stockholm'
          WEBPASSWORD={{ index .Data.data "password" }}
          ADMIN_EMAIL={{ index .Data.data "adminEmail" }}
          PIHOLE_DNS_="{{ env "NOMAD_IP_unbound" }}#{{ env "NOMAD_PORT_unbound" }};1.1.1.1;1.0.0.1"
          DNSSEC=true
          TEMPERATUREUNIT=c
          DHCP_ACTIVE=false
          {{ end }}
        EOH

        destination = "secrets/webui.env"
        env = true
      }

      service {
        name = "pi-hole-dns"
        port = "dns"

        tags = [
          "traefik.enable=true",

          "traefik.tcp.routers.pihole.entrypoints=tcpdns",
          "traefik.tcp.routers.pihole.rule=HostSNI(`*`)",

          "traefik.tcp.routers.piholetlsdns.entrypoints=tlsdns",
          "traefik.tcp.routers.piholetlsdns.rule=HostSNI(`pihole.hq.carboncollins.se`)",
          "traefik.tcp.routers.piholetlsdns.tls=true",
          "traefik.tcp.routers.piholetlsdns.tls.certresolver=lets-encrypt",
          "traefik.tcp.routers.piholetlsdns.tls.domains[0].main=pihole.hq.carboncollins.se",
          "traefik.tcp.routers.piholetlsdns.tls.options=tlsdns@file",

          "traefik.udp.routers.pihole.entrypoints=udpdns"
        ]
      }
    }

    task "unbound" {
      driver = "docker"

      lifecycle {
        hook = "prestart"
        sidecar = true
      }

      resources {
        cpu = 250
        memory = 300
      }

      config {
        image = "[[ .unboundImage ]]"

        ports = ["unbound"]

        volumes = [
          "local/unbound:/opt/unbound/etc/unbound/"
        ]
      }

      template {
        data = <<EOH
[[ fileContents "./config/unbound.template.conf" ]]
        EOH

        destination = "local/unbound/unbound.conf"
        change_mode = "noop"
      }
    }

    task "metrics" {
      driver = "docker"

      lifecycle {
        hook = "poststart"
        sidecar = true
      }

      resources {
        cpu = 250
        memory = 300
      }

      config {
        image = "[[ .jobDockerImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        cap_add = ["net_bind_service"]

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "jobs/network-internal-dns/tasks/metrics/telegraf" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN={{ index .Data.data "token" }}
          INFLUX_ORG={{ index .Data.data "organisation" }}
          INFLUX_BUCKET={{ index .Data.data "bucket" }}
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "30s"
    healthy_deadline = "5m"
    progress_deadline = "10m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
