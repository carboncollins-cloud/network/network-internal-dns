# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [2022-04-23]

### Added
- custom name for Nomad job

### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
- Pipeline template to new cloud variant
- pihole version to 2022.04.3
- unbound version to 1.15.0
- telegraf version to 1.22.1
